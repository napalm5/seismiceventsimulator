#!/bin/bash
#
#SBATCH --job-name=percolation
#SBATCH --output=/slurm_output/output.txt
#SBATCH --array=0-3

case $SLURM_ARRAY_TASK_ID in
    0) TICKER="cariati" ;;
    1) TICKER="ginosa" ;;
    2) TICKER="lentini" ;;
    3) TICKER="vallata" ;;
esac

srun hostname
srun echo "I am job $SLURM_ARRAY_TASK_ID"
srun python3.6 percolation.py $* -t $TICKER
