#!/bin/bash

mgnitude=''

print_usage() {
  printf "Usage: create_rupture_model.sh -m MAGNITUDE -d OUTPUT_DIRECTORY"
}

while getopts 'm:t:d:' flag; do
    case "${flag}" in
      t) ticker="${OPTARG}" ;;
      m) magnitude="${OPTARG}" ;;
      d) dir="${OPTARG}" ;;
      *) print_usage
       exit 1 ;;
  esac
done

sed "s/__MAG__/$magnitude/g" rupture_template.xml > $dir"rupture_model.xml";
sed "s/__TICKER__/$ticker/g" scenario_template.ini > $dir"scenario.ini";
#cp scenario_template.ini $dir"scenario.ini" ; 


exit 0 ;
