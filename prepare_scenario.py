#!/usr/bin/env python3

import shapely as shp
import geopandas as gpd
from shapely.geometry import Point
from shapely.geometry import Polygon

import osmnx as ox
import networkx as nx
ox.config(log_file=True, log_console=True, use_cache=True)
#from LatLon import LatLon

import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
import sys
from importlib import reload
import logging
#logging.getLogger(ox.__name__).setLevel(logging.WARNING)
ox_log=logging.getLogger(ox.__name__)
ox_log.addHandler(logging.NullHandler())
ox_log.propagate = False
from modules import functions as f
from modules import classes as c
from modules import places

par=f.check_args_pre(sys.argv[1:])
RESULTS_DIR=par.folder

place=places.places_dict[par.ticker]['place']
point=places.places_dict[par.ticker]['point']
distance=places.places_dict[par.ticker]['distance']
distance=8000
#point=(39.489971, 16.958957)

point=point[::-1] #output will be in LongLat format, not LatLong
tmpgdf=gpd.GeoDataFrame([Point(point)],columns=['geometry'],crs={'init': 'epsg:4326'})
tmpgdf=tmpgdf.to_crs({'init': 'epsg:32633'})

center=tmpgdf.geometry.values[0]
area=center.buffer(distance).envelope #square neighborhood of the initial point

xmin,xmax,ymin,ymax = min(area.exterior.coords.xy[0]), max(area.exterior.coords.xy[0]), min(area.exterior.coords.xy[1]), max(area.exterior.coords.xy[1])
width=200
Xleft = xmin
Xright = xmin + width
Ytop = ymax
Ybottom = ymax - width
Ytop_start = Ytop
polygons = []

while Xleft < xmax:
    while Ytop > ymin:
        polygons.append(Polygon([[Xleft,Ytop],[Xright,Ytop],[Xright,Ybottom],[Xleft,Ybottom]]))  
        Ytop-=width
        Ybottom-=width
    Xleft+=width
    Xright+=width
    Ytop=Ytop_start
    Ybottom=Ytop_start - width

polygons_gdf=gpd.GeoDataFrame(polygons, columns=['geometry'], crs={'init': 'epsg:32633'})  
polygons_gdf.to_file(RESULTS_DIR+'/area.shp')

centroids=[p.centroid for p in polygons]
centroids_gdf=gpd.GeoDataFrame(centroids, columns=['geometry'], crs={'init': 'epsg:32633'})
centroids_gdf_proj=centroids_gdf.to_crs({'init': 'epsg:4326'})
centroids_lonlat=[[c.x,c.y] for c in centroids_gdf_proj.geometry.values] 
centroids_lonlat_df=pa.DataFrame(centroids_lonlat, columns=['Lon','Lat'])
centroids_lonlat_df.to_csv(RESULTS_DIR+'/sites.csv', index=False, header=False)
