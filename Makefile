ID = 1
#RESULTS_DIR = "./run_"${ID}"/"
RESULTS_DIR = "./results/run_`date +'%y-%m-%d_%H-%M'`"
LOCATION = "cariati"
MAGNITUDE = 6
VPATH = ${RESULTS_DIR}
oq = "/home/claudio/Programs/Openquake/bin/oq"

.PHONY: run
run: %.area.shp %.gmf-data.csv %.sitemesh.csv
	python -i main.py -t ${LOCATION} -f ${RESULTS_DIR}

%.gmf-data.csv %.sitemesh.csv : %.rupture_model.xml %.scenario.ini %.sites.csv %.area.shp
	cd ${RESULTS_DIR}; \
	${oq} engine --run scenario.ini --exports csv; \
	perl-rename 's/_[0-9]+//s' *_*.csv

%.sites.csv %.area.shp :
	mkdir -p ${RESULTS_DIR}; \
	python prepare_scenario.py -t ${LOCATION} -f ${RESULTS_DIR}

%.rupture_model.xml %.scenario.ini :
	mkdir -p ${RESULTS_DIR}; \
	bash create_rupture_model.sh -t ${LOCATION} -m ${MAGNITUDE} -d ${RESULTS_DIR}/

.PHONY: clean
clean :
	cd ${RESULTS_DIR}; \
	rm *.cpg *.dbf *.prj *.shp *.shx events*.csv gmf*.csv sigma*.csv sitemesh*.csv *.xml *.ini sites.csv

.PHONY: clean_all
clean_all :
	rm -r run_*
