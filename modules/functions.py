import numpy as np
import scipy as sp
import pandas as pa
from sklearn.neighbors import KDTree
from scipy.integrate import simps
from matplotlib import pyplot as plt

import geopandas as gpd
import networkx as nx
import networkx.algorithms as na
import networkx.algorithms.approximation as naa
import osmnx as ox
import shapely as shp
from shapely.ops import cascaded_union
from shapely.geometry import Point
from rtree import index

import argparse
import os
import sys
import copy
import logging
import itertools as it
import multiprocessing
import time
import contextlib
logging.getLogger(ox.__name__).setLevel(logging.CRITICAL)


#---Load Data---
def get_topology(point,distance): #point in LatLon
    #G = ox.graph_from_place(place)
    G = ox.graph_from_point(point,distance = distance) #np
    #fig, ax  =  ox.plot_graph(G, node_size = 10) #network_type = 'drive'
    G = ox.project_graph(G,to_crs={'init':'epsg:32633'})
    G = ox.get_undirected(G)
    nx.set_edge_attributes(G,1,'damage')

    #Add expected travel time as edge attribute
    k = list(G.edges(keys = True))
    #v = list(np.random.rand(len(k))) #continuous damage
    with contextlib.redirect_stdout(None): e = ox.graph_to_gdfs(G, nodes=False, edges=True)
    speed_dict = {'primary':130,'secondary':100,'tertiary':70,'service':50,'residential':50, 'road':50}
    h = e.highway.replace(speed_dict)
    h = h.mask(h.apply(lambda x: type(x))==str,50)
    h = h.mask(h.apply(lambda x: type(x))==list,50)
    v = (e.length/(h*(1000/3600))).values
    d = dict(zip(k, v))
    nx.set_edge_attributes(G,d,'time')
    
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
#    buildings = ox.buildings_from_point(point,distance).to_crs(crs=edges.crs)
    
    return G,edges,nodes

def get_topology_from_scenario(scenario_gdf):
    area_polygon = shp.ops.unary_union(scenario_gdf.to_crs({'init': 'epsg:4326'}).geometry)
    area_boundaries = shp.geometry.mapping(area_polygon)['coordinates'][0]

    G = ox.graph_from_polygon(area_polygon)
    G = ox.project_graph(G,to_crs={'init':'epsg:32633'})
    G = ox.get_undirected(G)
    nx.set_edge_attributes(G,1,'damage')

    #Add expected travel time as edge attribute
    k = list(G.edges(keys = True))
    #v = list(np.random.rand(len(k))) #continuous damage
    with contextlib.redirect_stdout(None): e = ox.graph_to_gdfs(G, nodes=False, edges=True)
    speed_dict = {'primary':130,'secondary':100,'tertiary':70,'service':50,'residential':50, 'road':50}
    h = e.highway.replace(speed_dict)
    h = h.mask(h.apply(lambda x: type(x))==str,50)
    h = h.mask(h.apply(lambda x: type(x))==list,50)
    v = (e.length/(h*(1000/3600))).values
    d = dict(zip(k, v))
    nx.set_edge_attributes(G,d,'time')
    
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
#    buildings = ox.buildings_from_point(point,distance).to_crs(crs=edges.crs)
    
    return G,edges,nodes

    
def get_topology_real_damage(point,distance): #point in LatLon
    #G = ox.graph_from_place(place)
    G=ox.graph_from_point(point,distance=distance) #np
    #fig, ax = ox.plot_graph(G, node_size=10) #network_type='drive'
    G=ox.project_graph(G,to_crs={'init':'epsg:32633'})
    G=ox.get_undirected(G)
    nx.set_edge_attributes(G,1,'damage')

    #Add expected travel time as edge attribute
    k=list(G.edges(keys=True))
    #v=list(np.random.rand(len(k))) #continuous damage
    with contextlib.redirect_stdout(None): e=ox.graph_to_gdfs(G, nodes=False, edges=True)
    speed_dict={'primary':130,'secondary':100,'tertiary':70,'service':50,'residential':50, 'road':50}
    h=e.highway.replace(speed_dict)
    h=h.mask(h.apply(lambda x: type(x))==str,50)
    h=h.mask(h.apply(lambda x: type(x))==list,50)
    v=(e.length/h).values
    d=dict(zip(k, v))
    nx.set_edge_attributes(G,d,'time')
    
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
#    buildings = ox.buildings_from_point(point,distance).to_crs(crs=edges.crs)
    
    return G,edges,nodes

def get_topology_unsimplified(point,distance): #point in LatLon
    #G = ox.graph_from_place(place)
    G=ox.graph_from_point(point,distance=distance,simplify=False) #np
    #fig, ax = ox.plot_graph(G, node_size=10) #network_type='drive'
    G=ox.project_graph(G,to_crs={'init':'epsg:32633'})
    G=ox.get_undirected(G)
        
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
#    buildings = ox.buildings_from_point(point,distance).to_crs(crs=edges.crs)
    
    return G,edges,nodes

def get_topology_directed(point,distance): #point in LatLon
    #G = ox.graph_from_place(place)
    G=ox.graph_from_point(point,distance=distance) #np
    #fig, ax = ox.plot_graph(G, node_size=10) #network_type='drive'
    G=ox.project_graph(G,to_crs={'init':'epsg:32633'})
    nx.set_edge_attributes(G,1,'damage')

    #Add expected travel time as edge attribute
    k=list(G.edges(keys=True))
    #v=list(np.random.rand(len(k))) #continuous damage
    with contextlib.redirect_stdout(None): e=ox.graph_to_gdfs(G, nodes=False, edges=True)
    speed_dict={'primary':130,'secondary':100,'tertiary':70,'service':50,'residential':50, 'road':50}
    h=e.highway.replace(speed_dict)
    h=h.mask(h.apply(lambda x: type(x))==str,50)
    h=h.mask(h.apply(lambda x: type(x))==list,50)
    v=(e.length/(h*(1000/3600))).values
    d=dict(zip(k, v))
    nx.set_edge_attributes(G,d,'time')
    
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
#    buildings = ox.buildings_from_point(point,distance).to_crs(crs=edges.crs)
    
    return G,edges,nodes

def get_topology_unprojected(point,distance): #point in LatLon
    #G = ox.graph_from_place(place)
    G=ox.graph_from_point(point,distance=distance) #np
    #fig, ax = ox.plot_graph(G, node_size=10) #network_type='drive'
    G=ox.get_undirected(G)
        
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
#    buildings = ox.buildings_from_point(point,distance).to_crs(crs=edges.crs)
    
    return G,edges,nodes


def population_from_csv(csv_file, crs):
    """
    Returns GPD with the contents of the csv, projected to crs

    Paramters
    -----------
    csv_file: csv file in which the populations is written
    crs: crs in which the gpd will be projected (written as a dictionary)

    Returns
    ----------
    Gpd with the contents of the csv file
    """

    pop=pa.read_csv(csv_file,index_col=None) #Centroids and values of zone censuarie
    geometry=pop.apply(lambda x: Point(x.st_x,x.st_y),axis=1)
    pop['geometry']=geometry
    pop=gpd.GeoDataFrame(pop,geometry='geometry')
    pop.crs={'init': 'epsg:4326'}
    pop=pop.to_crs(crs)
    return pop

def population_from_txt(txt_file, crs): #returns GPD with the contents of the csv, projected to crs
    pop=pa.read_csv(txt_file,delim_whitespace=True) #Centroids and values of zone censuarie
    geometry=pop.apply(lambda i: Point(i.x,i.y),axis=1)
    pop['geometry']=geometry
    pop=gpd.GeoDataFrame(pop,geometry='geometry')
    pop.crs={'init' : 'epsg:32633'}
    pop=pop.to_crs(crs)
    return pop



#---GIS manipulation---

def add_interruptions(G,p_damage):
    # edges=ox.graph_to_gdfs(G, nodes=False, edges=True)
    # for i in range(int(p_damage*G.number_of_edges())):
    #     e=edges.sample()
    #     G.remove_edge(*e.u.values,*e.v.values)
    #     edges.drop(e.index,inplace=True)
    # Temporary arrangement
    G=add_interruptions_mono(G,p_damage)
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
    return G,edges,nodes

def add_interruptions_mono(G,p_damage):
    GG=G.copy()
    for e in G.edges:
        if np.random.uniform() < p_damage:
            GG.remove_edge(*e)
    
    # for i in range(int(p_damage*G.number_of_edges())):
    #     random_edge=list(G.edges)[np.random.choice(len(G.edges))]
    #     G.remove_edge(*random_edge)
    return GG


def add_damage(G,p_damage):
    k=list(G.edges(keys=True))
    #v=list(np.random.rand(len(k))) #continuous damage
    v=list(np.random.choice([1,1.2,1.5,2],len(k))) #discrete damage
    d=dict(zip(k, v))
    nx.set_edge_attributes(G,d,'damage')
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
    return G,edges,nodes

def add_gaussian_interruptions(G,edges,nodes=None,epicenter=None,radius=None,strength=None): #Maybe pass a copy?
    #Everything should be in the right coordinate system
    if epicenter is None:
        if nodes is None:
            with contextlib.redirect_stdout(None):
                nodes=ox.graph_to_gdfs(G, nodes=True, edges=False)
        x=np.random.uniform(nodes.x.max(),nodes.x.min())
        y=np.random.uniform(nodes.y.max(),nodes.y.min())
        epicenter=shp.geometry.Point((x,y))
    if radius is None:
        radius=np.random.randint(2000,4000)
        radius=6000
    if strength is None: strength=0.7
    
    for e in edges.iterrows():   #Edges GDF is not really needed, I think
        distance=linepoint_distance(e[1].geometry,epicenter)
        p_damage=GMPE(distance,radius,strength)
        if np.random.uniform(0,1)<p_damage: G.remove_edge(e[1].u,e[1].v)

    with contextlib.redirect_stdout(None): nodes,edges=ox.graph_to_gdfs(G, nodes=True, edges=True)
    return G,edges,nodes

def add_interruptions_from_scenario(G,scenario_gdf):
    #rtree courtesy of: https://stackoverflow.com/questions/14697442/faster-way-of-polygon-intersection-with-shapely/14804366

    def fragility_curve(pga):
        #possible coefficients: 28.6
        probability = 1-(1/(1+70*(pga**3))) #hand-fitted from poor empirical data
        return probability
    
    scenario_gdf['p_damage']=scenario_gdf['intensity'].apply(fragility_curve)
    idx = index.Index()
    cells=scenario_gdf.geometry.values
    for pos, cell in enumerate(cells):
        idx.insert(pos, cell.bounds)

    for u,v,line in G.copy().edges(data='geometry'):
        p_broken = 0
        
        #for n,row in scenario_gdf.iterrows()
        for pos in idx.intersection(line.bounds):
            i=line.intersection(cells[pos])
            p_broken += scenario_gdf.iloc[pos].p_damage*i.length

        p_broken = p_broken/line.length
        if np.random.uniform() < p_broken:
            G.remove_edge(u,v)
    
    #for edge in G
      #(calculate the prob of interruption)
      #create list of squares that intersect edge
      #average their probability of being broken
      #break edge with that probability

    return G

def add_risk(G):
    
    """
    Add risk attribute to a road graph, taken from GIS data
    
    Parameters
    ---------
    G: the original load graph
    
    Returns
    -------
    Networkx MultiGraph with a 'risk' attribute
    """

    
    danger_gpd=gpd.read_file('./data/Pericolosita/sez_cens.shp')
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
    risk_dict={}

    for u,v,g in G.edges(data='geometry'):
        weighted_sum=0
        weights_sum=0
        for n,p in danger_gpd.iterrows():
            intersection=p.geometry.intersection(g)
            weights_sum+=intersection.length
            weighted_sum+=p.ed_cro*intersection.length
        weighted_mean=weighted_sum/weights_sum if weights_sum != 0 else 0
        risk_dict.update({(u,v):weighted_mean})
    nx.set_edge_attributes(G,risk_dict,'risk')
    with contextlib.redirect_stdout(None): nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
    return G,nodes,edges

def add_damage_scenario(G):

    """
    Reproduce a scenario described in GIS data on a given road graph
    
    Parameters
    ---------
    G:the original road graph
    
    Returns
    -------
    A Networkx MultiGraph with less edges and a 'damage' attribute
    """
    return 0



def get_ES_df(ticker,crs):
    ES_df=pa.read_csv('./data/ES/ES_'+ticker+'.csv',index_col=None)
    geometry=ES_df.apply(lambda x: Point(x.st_x,x.st_y),axis=1)
    ES_df['geometry']=geometry
    ES_df=gpd.GeoDataFrame(ES_df,geometry='geometry')
    if ES_df.st_x.values[0]<100:
        ES_df.crs={'init': 'epsg:4326'}
    else:
        ES_df.crs={'init': 'epsg:32633'}
    ES_df=ES_df.to_crs(crs)
    return ES_df
    
def get_ES(ticker,crs,nodes=0):
    #returns a dict with the coordinates of the ES
    ES_df=get_ES_df(ticker,crs)
    ES={
        'ES1' : ES_df[ES_df.ES==1].geometry.values[0],
        'ES2' : ES_df[ES_df.ES==2].geometry.values[0],
        'ES3' : ES_df[ES_df.ES==3].geometry.values[0]
    }
    return ES

def get_ES_osmid(ticker,crs,nodes):
    #returns a dict with the osmid of the ES
    ES_df=get_ES_df(ticker,crs)
    ES={
        'ES1' : get_nearest_node(ES_df[ES_df.ES==1].loc[:,['st_y','st_x']].values[0],nodes),
        'ES2' : get_nearest_node(ES_df[ES_df.ES==2].loc[:,['st_y','st_x']].values[0],nodes),
        'ES3' : get_nearest_node(ES_df[ES_df.ES==3].loc[:,['st_y','st_x']].values[0],nodes)
    }
    return ES


    

def get_hospital(point,distance,nodes): #Point object with coords of hospital
    """
    Get position of the best positioned hospital in the area

    Parameters
    ---------
    point: center of the area considered
    distance: radius of the area considered
    nodes: Gpd containing the nodes of the road graph

    Returns
    -------
    Shapely Point with the coordinates of the hospital
    """
    
    #hospitals = ox.pois_from_place(place, amenities=['hospital'])
    hospitals = ox.pois_from_point(point,distance=distance, amenities=['hospital']).to_crs(crs=nodes.crs)
    hospital=hospitals.geometry.iloc[0]
    if type(hospital) is not shp.geometry.Point:
        hospital=hospital.centroid    
    hospital=Point(hospital.x,hospital.y)
    #hospital = Point(hospitals.geometry.values[0].x, hospitals.geometry.values[0].y)
    return hospital
    

def get_hospital_osmid(point,distance,nodes):
    """
    Get node nearest to the of the best positioned hospital in the area

    Parameters
    ---------
    point: center of the area considered
    distance: radius of the area considered
    nodes: Gpd containing the nodes of the road graph

    Returns
    -------
    Osmid of the node nearest to the hospital, in the given set of nodes
    """
    
    hospitals = ox.pois_from_point(point,distance=distance, amenities=['hospital']).to_crs(crs=nodes.crs)
    hospital=hospitals.geometry.iloc[0]
    if type(hospital) is not shp.geometry.Point:
        hospital=hospital.centroid    
    hospital=(hospital.y,hospital.x)
    #hospital = (hospitals.geometry.values[0].y, hospitals.geometry.values[0].x)
    
    # tree = KDTree(nodes[['y', 'x']], metric='euclidean')
    # hospital_idx = tree.query([hospital], k=1, return_distance=False)[0]
    # closest_node_to_hosp = nodes.iloc[hospital_idx].index.values[0]
    closest_node_to_hosp=get_nearest_node(hospital,nodes)
    return closest_node_to_hosp

def get_gaussian_victims(G,nodes=None,epicenter=None,radius=None,strength=None): 
        #Everything has to be in the right coordinate system
    if nodes is None:
        with contextlib.redirect_stdout(None):
            nodes=ox.graph_to_gdfs(G, nodes=True, edges=False)
    if epicenter is None:
        x=np.random.uniform(nodes.x.max(),nodes.x.min())
        y=np.random.uniform(nodes.y.max(),nodes.y.min())
        epicenter=shp.geometry.Point((x,y))
    if radius is None:
        radius=np.random.randint(2000,4000)
        radius=6000
    if strength is None: strength=0.05
    
    victims=gpd.GeoDataFrame([],crs=nodes.crs)
    
    for i,n in nodes.iterrows(): #Ideally I should iterate over buildings
        distance=n.geometry.distance(epicenter)
        p_damage=GMPE(distance,radius,strength)
        if np.random.uniform(0,1)<p_damage:
            n.loc['population']=np.random.randint(0,10)
            victims=victims.append(n,sort=False)

    return victims


def get_victims_from_scenario(scenario_df,pop):
    #for area in scenario_df
      #get number of people living in the area
      #create number of victims proportional to pop_area and damage_area
      #create random point inside area
      #add point and number to victims_df

    victims_df=gpd.GeoDataFrame(columns=['injured','geometry'])
    
    for n,pop_row in pop.iterrows():
        n_victims = int(pop_row.popolazione*0.004) # 0.0011 # 0.004
        x = pop_row.geometry.x
        y = pop_row.geometry.y
        for i in range(n_victims):
            location = Point(x+np.random.uniform(-300,300),
                           y+np.random.uniform(-300,300))
            victim = gpd.GeoDataFrame([[1,location]],columns=['injured','geometry'])
            victims_df = victims_df.append(victim,ignore_index=True)
    return victims_df

def get_nearest_node(point,nodes): # point as a tuple (y,x)
    if type(point)==shp.geometry.Point:
        point=(point.y,point.x)
    tree = KDTree(nodes[['y', 'x']], metric='euclidean')
    closest_node_idx = tree.query([point], k=1, return_distance=False)[0]
    closest_node_to_point = nodes.iloc[closest_node_idx].index.values[0]
    return closest_node_to_point


#------ Operation on geometries -------
def near_edges(edges,point):

    """
    retrieves all the edges in a graph that are in a neighborhood of a given point.
    Then orders it based on their distance
    
    Parameters
    ---------
    G:Road Graph
    point: center of the neighborhood
    
    Returns
    -------
    ordered list of near edges
    """

    point_neighborhood = point.buffer(500)
    
    #Check if there is an edge in a neighborhood of the target
    nearby_streets = edges[edges.geometry.apply(point_neighborhood.intersects)]
    if nearby_streets.shape[0]==0:
        return []

    nearby_edges=[(ns[1].u,ns[1].v) for ns in nearby_streets.iterrows()]
    nearby_streets = nearby_streets.geometry.values

    distances = [point.distance(ns) for ns in nearby_streets]
    nearby_edges=np.array(nearby_edges)[np.argsort(distances)]

    return nearby_edges

    
def project_on_road(point,edges):
    """
    Parameters
    ----------

    Returns
    -------
    point_onroad: projection of the given point on the closest edge, as a Shapely Point
    closest_street: the row of the edges GPD containing the closest edge
    """
    
    point_neighborhood = point.buffer(500)
    
    #Check if there is an edge in a neighborhood of the target
    nearby_streets = edges[edges.geometry.apply(point_neighborhood.intersects)]
    if nearby_streets.shape[0]==0:
        raise nx.exception.NetworkXNoPath
        #return 0

    #Find the street point that is the nearest to the target
    closest_street = nearby_streets.loc[nearby_streets.geometry.apply(point.distance).idxmin()]
    point_onroad, p = shp.ops.nearest_points(closest_street.geometry, point)

    return point_onroad, closest_street


def fetch_edge(edges,u,v):
    """
    Find edge in the edges dataframe based on the nodes it connects

    Parameters
    ----------

    Returns
    -------
    Geometry of the edge, as a Shapely LineString
    """
    edge_row=(edges[(edges['u']==u) & (edges['v']==v)]
        if len(edges[(edges['u']==u) & (edges['v']==v)].geometry.values)!=0
              else edges[(edges['v']==u) & (edges['u']==v)])
    return edge_row.geometry.values[0]

def intermediate_route_points(target,street,nearest_route_point):
    """
    Computes the route points between one extreme of an edge and a given point
    inside it

    Parameters
    ----------
    street (gdf): the edge 

    Returns
    -------
    list of Shapely Points

    """


    buff=target.buffer(0.0001) #shapely has rounding problems with interpolation
    split=list(shp.ops.split(street.geometry,buff))
    route_ls=split[
        np.argmin([ls.distance(nearest_route_point) for ls in split])]
    route_points=[shp.geometry.Point(p) for p in route_ls.coords]

    # #Debug
    # debug3=gpd.GeoDataFrame([street.geometry,nearest_route_point],columns=['geometry'],geometry='geometry')
    # debug2=gpd.GeoDataFrame(split,columns=['geometry'],geometry='geometry')
    # debug1=gpd.GeoDataFrame([route_ls,target,nearest_route_point],columns=['geometry'],geometry='geometry')
    # ax=debug1.plot(zorder=1)
    # ax=debug2.plot(zorder=2,ax=ax,color='r')
    # ax=debug3.plot(zorder=3,ax=ax,color='green')
    # plt.show()
    
    return route_points

def closest_node_to_point(G,point):
    
    """
    Get the node of a graph closest to a given set of coordinates
    
    Parameters
    ---------
    point: Shapely Point with the coordinates of the point
    G: Networkx Graph containing the set of nodes 
    
    Returns
    -------
    The osmid of the opportune node
    """

    osmid=list(G.nodes)[0]
    dist=1000000
    
    for n,dic in G.nodes(data=True):
        geom=Point([dic['x'],dic['y']])
        if geom.distance(point)<dist:
            dist=geom.distance(point)
            osmid=n

    return n

def get_node_geometry(G,osmid):
    """
    Auto-explicative
    """
    return Point([G.nodes[osmid]['x'],G.nodes[osmid]['y']])

#---Operations on graphs---
def add_random_cycle(SG,G):
    """
    Add redundancy to a Steiner Tree, picking loops from a Graph that contains it

    Parameters
    ---------
    SG:Steiner Tree, as nx.MultiGraph
    G: Graph containing the Steiner Tree, as nx.MultiGraph

    Returns
    -------
    A tuple containing the list of nodes added and an nx.Graph encoding 
    the Extended Steiner Tree, now with some redundant paths.
    """

    ESG=SG.copy()
    random_node=np.random.choice(SG.nodes)
    random_cycle=na.find_cycle(G,random_node)
    
    #Extend the nodes of the Steiner Tree
    additional_nodes=[e[:-1] for e in random_cycle]
    additional_nodes=np.unique(np.array(additional_nodes).flatten())
    for n in additional_nodes:
        ESG.add_node(n,**G.nodes[n])    

    #Extend the edges of the Steiner Tree
    for i,j,k in random_cycle:
        ESG.add_edge(i, j,**G[i][j][k])
        try:
            ESG.add_edge(random_node, j,**u.G[random_node][j][0]) ##The key should be randomly extracted
        except:
            pass
        try:
            ESG.add_edge(i, random_node,**u.G[i][random_node][0])
        except:
            pass

    #Connect the Extended Steiner tree, if it isn't already connected
    if not nx.is_connected(ESG):
        ESG= max(nx.connected_component_subgraphs(ESG), key=len)


    return ESG

def add_random_branch(SG,G):
    
    """
    Add redundancy to a Steiner Tree, adding random connected edges from
    the main Graph

    Parameters
    ---------
    SG:Steiner Tree, as nx.MultiGraph
    G: Graph containing the Steiner Tree, as nx.MultiGraph

    Returns
    -------
    A tuple containing the list of nodes added and an nx.Graph encoding 
    the Extended Steiner Tree, now with some redundant branches.
    """

    ESG=SG.copy()
    added_something=False

    while added_something == False:
        random_node=np.random.choice(SG.nodes)
        edges=list(G.edges(random_node))
        orig_nodes=list(SG.nodes)
        #orig_nodes.remove(random_node)

        first_iteration=True
        
        while np.random.uniform()<0.7 or first_iteration==True:
            first_iteration=False
        
            edges=list(G.edges(random_node))
            edges=[e for e in edges if (not e[0] in orig_nodes) or (not e[1] in orig_nodes)]
            if len(edges)==0: break
            random_edge=edges[np.random.choice(len(edges))]
            
            #Extend the nodes of the Steiner Tree
            new_node=[i for i in random_edge if i != random_node]
            if type(new_node)==list: new_node=new_node[0]
            ESG.add_node(new_node,**G.nodes[new_node])    
            random_node=new_node
            orig_nodes.append(random_node)
            
            #Extend the edges of the Steiner Tree
            i,j=random_edge
            ESG.add_edge(i, j,**G[i][j][0])

            added_something=True
    
    return ESG

def chose_path(paths,return_list):
    
    """
    Randomly choses a path among all the possible paths connecting two points,
    written to be used inside parallel processes
    
    
    Parameters
    ---------
    paths: generator that contains all the possible paths
    return_list: list that will contain the chosen path 
                 (for parallelization purposes)
    
    Returns
    -------
    
    """

    paths_shortlist=np.unique([next(paths) for i in range(4)])
    for n in paths_shortlist[np.random.choice(len(paths_shortlist))]:
        return_list.append(n)

def add_random_parallel_path(SG,G):
    
    """
    Add redundancy to a Steiner Tree, adding random connected edges from
    the main Graph that form parallel paths with respect to the original
    tree.

    Parameters
    ---------
    SG:Steiner Tree, as nx.MultiGraph
    G: Graph containing the Steiner Tree, as nx.MultiGraph

    Returns
    -------
    A tuple containing the list of nodes added and an nx.Graph encoding 
    the Extended Steiner Tree, now with some redundant branches.
    """

    MultiG=G.copy()
    G=make_mono(G)
    ESG=SG.copy()
    added_something=False
    orig_nodes=list(SG.nodes)
    paths=[]
    additional_path=[]
    
    random_node1=np.random.choice(orig_nodes)
    orig_nodes.remove(random_node1)
    random_node2=np.random.choice(orig_nodes)
#    paths=it.cycle(nx.all_simple_paths(G,random_node1,random_node2,cutoff=int(len(G.edges)/2)))
    paths=it.cycle(nx.shortest_simple_paths(G,random_node1,random_node2,weight='length'))

    manager = multiprocessing.Manager()
    additional_path=manager.list()
    p = multiprocessing.Process(target=chose_path, name="Chose", args=(paths,additional_path))

    p.start()
    
    # Wait a maximum of 10 seconds for foo
    # Usage: join([timeout in seconds])
    p.join(20)
    # If thread is active
    if p.is_alive():
        print("Path generator is stuck, it will be killed")
        p.terminate()
        p.join()
    
    #paths=[next(paths)]
    #paths=[next(paths) for i in range(100)]
    #paths=np.unique([next(paths) for i in range(100)])
    #additional_path=np.random.choice(paths)

    additional_path=list(additional_path)

    #Extend the nodes of the Steiner Tree
    for n in additional_path:
        ESG.add_node(n,**MultiG.nodes[n])    

    #Extend the edges of the Steiner Tree
    for i,j in nx.utils.pairwise(additional_path):
        ESG.add_edge(i, j,**MultiG[i][j][0])
    
    return ESG

def make_mono(MultiG):
    
    """
    Return a Graph without multiple edges, obtained by keeping 
    only the edges with shortest length value.
    Inspired by: https://stackoverflow.com/questions/15590812/networkx-convert-multigraph-into-simple-graph-with-weighted-edges
    
    Parameters
    ---------
    DiG:A MultiGraph
    
    Returns
    -------
    G: A Graph
    """
    

    G = nx.Graph()
    G.graph=MultiG.graph
    for i,j,key,data in MultiG.edges(data=True,keys=True):
        w = data['length'] if 'length' in data else 0
        if G.has_edge(i,j):
            if G[i][j]['length'] > data['length']:
                nx.set_edge_attributes(G,{(i,j):data})
        else:
            G.add_node(i,**MultiG.nodes[i])
            G.add_node(j,**MultiG.nodes[j])
            G.add_edge(i, j,**MultiG[i][j][key])

    return G

def extend_tree(SG,G,n):
    ESG=SG.copy()
    for i in range(n): ESG=add_random_branch(ESG,G)
    for i in range(n): ESG=add_random_parallel_path(ESG,G)
    for i in range(n): ESG=add_random_cycle(ESG,G)

    return ESG

def meshedness_coefficient(G):
    n=float(len(G.nodes))
    m=float(len(G.edges))

    return ((m-n+1)/(2*n-5))

#---Percolation---
def p_percolation(G,start,p_damage):    
    n_targets=100
    n_successes=0
    for i in range(n_targets):
        G,nodes,edges=add_interruptions(G,p_damage)
        if nx.has_path(G,start,np.random.choice(G.nodes)): n_successes+=1
    return n_successes/n_targets

def p_percolation_better(G_orig,start,end_list,p_damage,n_iterations=1):
    """
    Calculates percolation on random targets(nodes)
    
    Parameters
    ----------
    start: osmid of start node
    end_list: list containing osmids of target
    
    Returns
    -------
    Fraction of reachable random targets
    """
    n_targets=len(end_list)
    n_successes=0
    robustnesses=[]
    fig, ax = plt.subplots(figsize=(6, 5))
    
    for i in range(n_iterations):
        G=G_orig.copy()
        G_dam,edges,nodes=add_interruptions(G,p_damage)

        # if p_damage>0.2:
        #     ax=edges.plot(ax=ax,zorder=1,color='grey')
        #     ax=nodes[nodes.osmid==start].plot(ax=ax,zorder=3,color='blue')
        #     ax=nodes[nodes.osmid.isin(end_list)].plot(ax=ax,zorder=2,color='red')
        #     fig.show()
        
        n_successes=0
        for i in end_list:
            if nx.has_path(G,start,i): n_successes+=1
        robustnesses.append(n_successes/n_targets)

    return np.average(robustnesses)



    for i in range(n_targets):
        G,nodes,edges=add_interruptions(G,p_damage)
        if nx.has_path(G,start,np.random.choice(G.nodes)): n_successes+=1
    return n_successes/n_targets

def percolation_with_population(G,start,p_damage,population):
    n_iterations=2 #debug
    with contextlib.redirect_stdout(None): nodes=ox.graph_to_gdfs(G, nodes=True, edges=False)
    pop_coords=[np.array(i[1].geometry.xy).flatten()[::-1] for i in population.iterrows()]
    pop_nodes=[get_nearest_node(p,nodes) for p in pop_coords]
    weights=population.popolazione.values
    robustnesses=[]
    print(pop_nodes)
    
    for i in range(n_iterations):
        G_dam,edges,nodes=add_interruptions(G,p_damage)
        n_successes=0
        for i,w in zip(pop_nodes,weights):
            if nx.has_path(G,start,i): n_successes+=w
        robustnesses.append(n_successes/weights.sum())

    return np.average(robustnesses)

def connectivity_pt(G_orig,start_list,p_damage,max_rescue_time,population,reps=1):
    G=copy.deepcopy(G_orig)
#    G,edges,nodes=add_damage(G,0) #damage should be added in the simulation class
    n_iterations=reps
    with contextlib.redirect_stdout(None): nodes=ox.graph_to_gdfs(G, nodes=True, edges=False)
    pop_coords=[np.array(i[1].geometry.xy).flatten()[::-1] for i in population.iterrows()]
    pop_nodes=[get_nearest_node(p,nodes) for p in pop_coords]
    weights=population.popolazione.values
    robustnesses=[]
    robustness=[]
    speed=14*2 #m/s

    if type(start_list) is not list: start_list=[start_list]
    
    for start in start_list:
        for i in range(n_iterations):
            G_dam,edges,nodes=add_interruptions(copy.deepcopy(G),p_damage)
            #G_dam,edges,nodes=add_damage(G,p_damage)

            edges_orig=ox.graph_to_gdfs(G,edges=True,nodes=False)
            ax=edges_orig.plot(color='red',zorder=1)
            ax=edges.plot(color='green',ax=ax,zorder=2)
            ax=population.plot(ax=ax,color='r',zorder=3)
            plt.show()
            
            n_successes=0
            
            for n,w in zip(pop_nodes,weights):
                if nx.has_path(G_dam,start,n):
                    reach_time=0 #seconds
                    route=nx.shortest_path(G_dam,start,n)
                    ls_list=[]
                    for u,v in zip(route,route[1:]):
                        road_df=edges[(edges['u']==u) & (edges['v']==v)] if len(edges[(edges['u']==u) & (edges['v']==v)].geometry.values)!=0 else edges[(edges['v']==u) & (edges['u']==v)]
                        #edges[(edges['from']==u) & (edges['to']==v)]
                        reach_time+=(road_df.length.values[0]*road_df.damage.values[0])/speed

                    print('raggiunto in: ',reach_time/60)
                    if reach_time<(max_rescue_time*60):
                        n_successes+=w
                        print('RAGGIUNTO')
                    else: print('NON RAGGIUNTO')
            robustnesses.append(n_successes/weights.sum())
#        robustness.append(robustnesses)
        robustness.append(np.average(robustnesses))
        robustnesses=[]

#    return robustness
    return np.average(robustness)


def connectivity_gpt(G,start,max_rescue_time,distance=6000):

    nodes,edges=ox.graph_to_gdfs(G,nodes=True,edges=True)

    epicenter=random_epicenter(nodes)
    radius=np.random.randint(int(distance*0.1),int(distance*2))
    strength=np.random.uniform(0.1,1.)

    victims=get_gaussian_victims(G,nodes,epicenter,radius,strength*0.1)
    if victims.shape==(0,0): return np.nan,np.nan
    pop_nodes=victims.osmid.values
    weights=victims.population.values
    weights=np.ones(weights.size)
    
    speed=14*1.2 #m/s  #50*1.2 km/h

    G_dam,edges,nodes=add_gaussian_interruptions(copy.deepcopy(G),edges,nodes,epicenter,radius,strength)

    if type(start) is list: start=start[0]
    n_successes=0

    for n,w in zip(pop_nodes,weights):
        if nx.has_path(G_dam,start,n):
            reach_time=0 #seconds
            route=nx.shortest_path(G_dam,start,n)
            ls_list=[]
            for u,v in zip(route,route[1:]):
                road_df=edges[(edges['u']==u) & (edges['v']==v)] if len(edges[(edges['u']==u) & (edges['v']==v)].geometry.values)!=0 else edges[(edges['v']==u) & (edges['u']==v)]
                reach_time+=(road_df.length.values[0]*road_df.damage.values[0])/speed

            if reach_time<(max_rescue_time*60):
                n_successes+=w

    x=(len(G.edges)-len(G_dam.edges))/len(G.edges)
    y=n_successes/weights.sum()

    # ax=ox.graph_to_gdfs(G, nodes=False,edges=True).plot(color='grey',zorder=1)
    # ax=edges.plot(color='green',ax=ax,zorder=2)
    # ax=victims.plot(color='red',ax=ax,zorder=3)
    # ax=nodes[nodes.osmid==start].plot(color='blue',ax=ax,zorder=3)
    # plt.show()
    
    return x,y


def resilience_indicator(ESG,nodes):

    ESG=make_mono(ESG)
    plot_dict={}
    n_iterations=150
    G=ESG.copy()
    
    for p in [0.01,0.05,0.1,0.15,0.2,0.25,0.3,0.5]:
        plot_value=0
        for i in range(n_iterations):
            ESG=add_interruptions_mono(G,p)
        
            for c in na.connected_components(ESG):
                comp=ESG.subgraph(c)
                #ox.plot_graph(nx.MultiGraph(comp))
                st=naa.steiner_tree(comp,nodes)
                if len(st) > 0:
                    plot_value+=1
        plot_value=plot_value/n_iterations
        plot_dict.update({p:plot_value})

    return auc(list(plot_dict.keys()),list(plot_dict.values()))

#---Auxiliary functions---
def auc(x,y):
    return simps(y,x)

def population_centroid(pop): #return a shapely.Point
    x=(pop.st_x*pop.popres).sum()/pop.popres.sum()
    y=(pop.st_y*pop.popres).sum()/pop.popres.sum()
    return (x,y)

def random_epicenter(nodes):
    x=np.random.uniform(nodes.x.max(),nodes.x.min())
    y=np.random.uniform(nodes.y.max(),nodes.y.min())
    epicenter=shp.geometry.Point((x,y))
    return epicenter

def nodes_centroid(nodes):
    x=(nodes.x.max()+nodes.x.min())/2
    y=(nodes.y.max()+nodes.y.min())/2
    centroid=shp.geometry.Point((x,y))
    return centroid

def linepoint_distance(line,point): # LineString, Point
    precision=10 #distance between line subdivisions, in meters
    length=line.length
    ratio=int(length/precision)
    line_subdivision=shp.ops.MultiPoint([line.interpolate((precision*i), normalized=False) for i in range(0, ratio)])
    distances=[point.distance(i) for i in line_subdivision]
    return np.average(distances)
                 
def GMPE(distance,radius,strength):
    return strength*np.exp(-((distance**2)/(radius**2)))

def n_injured(position,population,epicenter,radius):
    """
    Calculates how many victims there are in a building
    
    Parameters
    ----------
    position: Shapely Point with the coordinates of the centroid of the building
    population: number of inhabitants of the building

    Returns
    -------
    Int representing the number of injured people
    """

    distance=position.distance(epicenter)
    p=GMPE(distance,radius,0.4) #strenght is hardcoded for now
    n_injured=sp.stats.binom.rvs(p=p,n=population)
    return n_injured
    

#---Plotting functions---
def plot_traj(traj_gpd,edges,nodes):#,buildings):
    ax = edges.plot(linewidth=0.75, color='gray')
#    ax = buildings.plot(ax=ax, facecolor='khaki', alpha=0.7)
    ax=traj_gpd.plot(ax=ax,color='red',markersize=5)
    plt.show()

def animate_traj(traj_gpd,edges,nodes):#,buildings):
    figure_width=800
    figure_height=600
    #plt.figure(figsize=(figure_width,figure_height))
    for i in range(traj_gpd.shape[0]):
        if i%3==0:
            ax = edges.plot(linewidth=0.75, color='gray')
    #        ax = nodes.plot(ax=ax, markersize=2, color='gray')
#            ax = buildings.plot(ax=ax, facecolor='khaki', alpha=0.7)
#            ax = nodes[nodes['osmid']==start].plot(ax=ax,color='red',markersize=80) #hospital
#            ax = nodes[nodes['osmid']==end].plot(ax=ax,color='blue',markersize=80) #patient
            ax = traj_gpd.iloc[0:1].plot(ax=ax,color='red',markersize=80) #hospital
            ax = traj_gpd.iloc[-1:-2:-1].plot(ax=ax,color='blue',markersize=80) #patient
            

            ax = traj_gpd.iloc[(i-1):i].plot(ax=ax,color='red')
            plt.axis('off')
            filepath = os.path.join('./trajectory/', 'traj.'+str(i)+'.png')
            plt.savefig(filepath,dpi=200)


#---Argument parsing and stuff---
def dprint(string,level=1):
    if level<=debug:
        print(string)

def suppressOutput(func): #wrapper to suppress output of a function (doesn't work)
    def wrapper(*args, **kwargs):
        with open(os.devnull,"w") as devNull:
            original = sys.stdout
            sys.stdout = devNull
            func(*args, **kwargs)
            sys.stdout = original
    return wrapper
#e_r.EventRegistry = supressOutput(e_r.EventRegistry)
#ox.graph_to_gdfs = suppressOutput(ox.graph_to_gdfs)

@contextlib.contextmanager
def redirectOut(): #redirects stdout to stderr (context)
    sout = sys.stdout
    sys.stdout = sys.stderr
    yield
    sys.stdout = sout

def check_args(args):
    parser=argparse.ArgumentParser(description='Implementation of financial predictive algorithms')
    parser.add_argument('-e', '--es', help='Which ES to consider', type=str, default='2')
    parser.add_argument('-r', '--repetitions', help='Number of repetitions of the experiment', type=int, default=100)
    parser.add_argument('-t', '--ticker', help='Where to place the simulation', type=str, default=None)
    parser.add_argument('-m', '--minutes', help='Time lmit to save a person', type=int, default=45)
    parser.add_argument('-c', '--causal', action='store_true', help='Use causal damage')

    parser.add_argument('-f', '--folder', help='Where to read input and write output', default='./results_tmp')
    parser.add_argument('-i', '--oqid', help='ID of the OpenQuake run', default='1', type=str)
    parser.add_argument('-a', '--area', help='File that contains the area input (shp)', default='area.shp')

    parser.add_argument('-n', '--nooutput', action='store_true', help='Do not print output on file')
    parser.add_argument('-p', '--plot', action='store_true', help='Plot the results')
    parser.add_argument('-d', '--debug', help='Set debug info verbosity', type=int, default=0)
    return parser.parse_args()

def check_args_pre(args):
    parser=argparse.ArgumentParser(description='Implementation of financial predictive algorithms')
    parser.add_argument('-t', '--ticker', help='Where to place the simulation', type=str, default=None)
    parser.add_argument('-f', '--folder', help='Where to write output', default='./results_tmp/')
    parser.add_argument('-d', '--debug', help='Set debug info verbosity', type=int, default=0)
    return parser.parse_args()
