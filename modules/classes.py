from random import sample

import numpy as np
from matplotlib import pyplot as plt
import pandas as pa

import shapely as shp
import geopandas as gpd
import networkx as nx
import osmnx as ox

import os
import contextlib
import traceback
import pdb

from modules import functions as f

class agent():
    def __init__(self,G,edges,nodes,start):
        self.G,self.edges,self.nodes=G,edges,nodes
        # if type(start)==shp.geometry.point.Point: #Agent starts from nearest node to 'start'
        #     nearest_node=f.get_nearest_node(np.array(start.xy).flatten()[::-1],nodes)
        #     self.geometry=nodes[nodes.osmid==nearest_node].geometry.iloc[0]
        # elif type(start)==int:
        #     self.geometry=nodes[nodes.osmid==start].geometry.iloc[0]
        if type(start)==list:  #accept starting point as tuple of coordinates
            self.geometry=shp.geometry.point.Point(start)
        else:
            self.geometry=start
        #Parameters
        self.stride_on_road=50
        self.stride_off_road=49
        self.stride_ratio=self.stride_on_road/self.stride_off_road
            
        self.onroad=False
        self.current_edge=None
        self.stride=self.stride_on_road  #100 on roads, 25 on off-road
        self.hasobjective=False
        self.objectives=gpd.GeoDataFrame([],columns=['geometry'],crs=nodes.crs)
        self.route=[]
        self.route_points=[]
        self.next_points=[]
        self.traj=[]
        self.i=0
        #traj_gpd=gpd.GeoDataFrame(traj,geometry='geometry', crs=nodes.crs, columns=['geometry'])


    def set_objective(self,objective):
        """
        Parameters
        ---------
        objective: one row of a GPD containing all the information about the objective

        Returns
        -------
        WIP
        """
        
        #self.objective['init_step']=

        objective.loc['success']=False
        try: self.objectives=self.objectives.append(objective,sort=False,verify_integrity=True)
        except ValueError: pass

        #This shouldn't be necessary
        #self.objectives=self.objectives.drop_duplicates(['geometry'])

        #creo lista di edges entro 500m dall'obiettivo
        #La ordino in base alla distanza (distanza minima)
        #itero sulla lista finchè non trovo un edge raggiungibile
        #Se lo trovo procedo con il routing usuale, se no esco con NoPath
        near_edges=f.near_edges(self.edges,objective.geometry)
        reachable=False
        i=0
        for e in near_edges:
            reachable = nx.has_path(self.G,f.closest_node_to_point(self.G,self.geometry),e[0])
            if reachable == True: break

        if reachable == False:
            # ax=self.edges.plot(color='grey',zorder=1)
            # #ax=objective.plot(ax=ax,color='red',zorder=2)
            # ax=gpd.GeoDataFrame([self.geometry,objective.geometry],columns=['geometry'],crs=self.nodes.crs).plot(
            #     ax=ax,zorder=2,color='blue')
            # plt.show()
            f.dprint('TOO FAR FROM ROAD',1)
            raise nx.exception.NetworkXNoPath
        
        try:
            self.route,self.start_points,self.end_points=get_route(
                self.geometry,self.objectives.iloc[-1].geometry,self.G,self.edges,self.nodes)
            
            # graph=self.G.to_undirected()
            # self.route=nx.shortest_path(graph,
            #                             f.get_nearest_node(self.xy[::-1],self.nodes),
            #                             f.get_nearest_node((objective.geometry.y,objective.geometry.x),self.nodes))
            #                             #,weight='damage')
            # self.route=[0,*self.route]
        except:
            self.hasobjective=False
            f.dprint('NO ROUTE',1)
            # traceback.print_exc()
            # print('AAAA')
            # ax=self.edges.plot(color='grey',zorder=1)
            # ax=self.objectives.iloc[-1:-2:-1].plot(ax=ax,color='red',zorder=2)
            # ax=gpd.GeoDataFrame([self.geometry],columns=['geometry'],crs=self.nodes.crs).plot(
            #     ax=ax,zorder=2,color='blue')
            # plt.show()
            raise nx.exception.NetworkXNoPath
            #return False
        #self.route_points=route_to_rp(self.route,self.edges)
        #if self.route_points[0].distance(self.geometry)>self.route_points[-1].distance(self.geometry):
        #    self.route_points=self.route_points[::-1]
        #if self.route_points[0]==self.geometry: self.route_points.pop(0)
        self.next_points=self.start_points
        
        self.onroad=False
        p,self.current_edge=f.project_on_road(self.geometry,self.edges)
        if not self.current_edge.geometry.intersects(self.geometry.buffer(0.0001)): self.stride=self.stride_on_road
        else: self.stride=self.stride_off_road
        
        self.hasobjective=True
        f.dprint('Objective set!',1)
        return True
    
    def step(self,G_true,global_step=-1):
        if self.hasobjective==False: return True
        stride=self.stride
        pos=self.geometry
        self.traj.append(self.geometry)

        while stride != 0:
            #print(self.route)
            f.dprint('Max stride: '+str(self.stride),1)
            f.dprint('Current stride: '+str(stride),1)
            if len(self.next_points) != 0:
                if debug >= 1:
                    tmp=[pos,*self.next_points]
                    # tmp=gpd.GeoDataFrame(tmp,geometry='geometry', crs=self.nodes.crs, columns=['geometry'])
                    # edges_true=ox.graph_to_gdfs(G_true,nodes=False,edges=True)
                    # ax=edges_true.plot(color='grey',zorder=1)
                    # ax=self.edges.plot(column='oneway',zorder=1)
                    # ax=self.objectives.plot(color='orange',zorder=2,ax=ax)
                    # tmp.plot(ax=ax,zorder=2)
                    # plt.show()
                if pos.distance(self.next_points[0])>stride:
                    f.dprint('not reached, distance = '+str(pos.distance(self.next_points[0])),1)
                    ls=shp.geometry.LineString((pos,self.next_points[0]))
                    pos=ls.interpolate(stride)
                    stride=0
                else:
                    f.dprint('reached, distance = '+str(pos.distance(self.next_points[0])),1)
                    stride-=pos.distance(self.next_points[0])

                    if (self.onroad==False
                    and (not self.current_edge.geometry.intersects(pos.buffer(0.0001))
                         or not self.current_edge.geometry.intersects(self.next_points[0].buffer(0.0001)))
                    ):
                        if self.stride==self.stride_on_road: stride=stride/self.stride_ratio
                        self.stride=self.stride_off_road
                    else:
                        if self.stride==self.stride_off_road: stride=stride*self.stride_ratio
                        self.stride=self.stride_on_road
                        
                        
                    pos=self.next_points.pop(0)
                                                 
            else:
                #self.route=self.route[1:]
                if len(self.route)<2:
                    if len(self.end_points) > 0:
                        self.onroad=False
                        p,self.current_edge=f.project_on_road(self.objectives.iloc[-1].geometry,self.edges)
                        self.next_points=self.end_points
                    else:
                        self.hasobjective=False
                        i=self.objectives.index[-1] 
                        self.objectives.loc[i,'success']=True
                        return True
                else:
                    if self.route[:2] not in G_true.edges: #If next node is actually reachable, report it to central
                        print('NOT IN EDGES')
                        self.G.remove_edge(self.route[0],self.route[1])
                        try:
                            is_reachable=self.set_objective(self.objectives.copy().iloc[-1])
                        except: #nx.exception.NetworkXNoPath:
                            print('Target unreachable!')
                            self.hasobjective=False
                            return True
                        return self.G
                    else:
                        #ox.plot_graph_route(self.G,self.route,show=True)
                        #self.traj_df().plot(color='blue')
                        #plt.show()
                        self.next_points=route_to_rp(self.route[:2],self.edges)
                        ##EXPERIMENTAL##
                        ##should probably check with the nodes list
                        ## Se le coordinate di route[0] sono diverse da route_points[0]
                        if (f.get_node_geometry(self.G,self.route[0]).distance(self.next_points[0])>
                           f.get_node_geometry(self.G,self.route[1]).distance(self.next_points[0])):
                            self.next_points=self.next_points[::-1]
                        #if f.closest_node_to_point(self.geometry,self.G) != self.route[0]:
                        #if f.get_node_geometry(self.G,self.route[0]).distance(self.next_points[0])>0.1:
                        #    self.next_points=self.next_points[::-1]
                        self.route.pop(0)
                        self.onroad=True
                        self.current_edge=False
                        # dis0=pos.distance(self.next_points[0])
                        # dis1=pos.distance(self.next_points[1])
                        # if dis0>dis1:
                        #     self.next_points=self.next_points[::-1]
            self.geometry=pos
#        self.traj.append(self.geometry)
        self.plot_traj()
        return False        

    def update_map(self,G_true):
        self.G=G_true
        self.edges,self.nodes=ox.graph_to_gdfs(G, nodes=True, edges=True)
        self.set_objective(self.objectives.copy().osmid.values[-1])

    def plot_traj(self,edges_true=None):
        ax=self.edges.plot(color='grey',zorder=1,column='oneway')
        if edges_true is not None: ax=edges_true.plot(ax=ax,cmap='Greens',column='oneway',zorder=2)#color='green')
        ax=self.traj_df.plot(ax=ax,markersize=8,zorder=3)#,cmap='Greens')
        ax=self.objectives.plot(ax=ax,markersize=30,color='red',zorder=3)
        plt.xlim(self.xy[0]-2000,self.xy[0]+2000)
        plt.ylim(self.xy[1]-1000,self.xy[1]+1000)
        #plt.figure(figsize=(15,15))
        plt.rcParams["figure.figsize"] = [16,9]
        plt.savefig('debug/traj_'+str(self.i)+'.png')
        plt.close()
        self.i+=1
        
    #Properties
    @property
    def traj_df(self):
        return gpd.GeoDataFrame(self.traj,geometry='geometry', crs=self.nodes.crs, columns=['geometry'])

    @property
    def xy(self):
        return np.array(self.geometry.xy).flatten()

    @property
    def osmid(self):
        return f.get_nearest_node(self.xy[::-1],self.nodes)


####
        
def route_to_rp(route,edges):
    ls_list=[]
    for u,v in zip(route,route[1:]):
        ls=edges[(edges['u']==u) & (edges['v']==v)].geometry.values[0] if len(edges[(edges['u']==u) & (edges['v']==v)].geometry.values)!=0 else edges[(edges['v']==u) & (edges['u']==v)].geometry.values[0]
        ls_list.append(ls)

    route_line=shp.ops.linemerge(ls_list)
    route_geom = gpd.GeoDataFrame([[route_line]], geometry='geometry', crs=edges.crs, columns=['geometry'])
    route_points=[shp.geometry.Point(p) for p in route_line.coords]

#    g=gpd.GeoDataFrame(route_points,geometry='geometry', columns=['geometry'])
#    g.plot()
#    plt.show()

    # ax=edges.plot()
    # route_geom.plot(ax=ax,color='red',markersize=20)
    # plt.show()
    return route_points


class simulation:
    """
    Class that manages the whole rescue operation
    """

    def __init__(self,point=None,distance=None,ticker='',p_interruptions=0.,p_damage=0.,
                 epicenter=None,
                 radius=None,
                 scenario_gdf=None,
                 strength=0.4,
                 seismic_pars=None):
        """
        Arguments
        ---------
        point: coordinates of the center of the simulation, as a tuple
        epicenter: coordinate of epicenter, as a Shapely Point
        """

        #Simulation parameters
        self._collision_distance=0
        self.point=point   
        self.distance=distance
        self.n_objectives=2
        self.n_rescuers=1
        self.seismic_pars=seismic_pars
        
        #Important quantities
        self.area=shp.ops.unary_union(scenario_gdf.geometry)
        self.scenario_gdf=scenario_gdf
        
        #Map
        ### Topology should be determined by scenario_gdf
        #self.G,self.edges,self.nodes=f.get_topology(point,distance)
        #self.G,self.edges,self.nodes=f.add_risk(self.G)
        self.G,self.edges,self.nodes=f.get_topology_from_scenario(scenario_gdf)
        
        self.G_orig=self.G.copy()
        self.nodes_orig,self.edges_orig=ox.graph_to_gdfs(self.G_orig, nodes=True, edges=True)
        #self.G_true,self.edges_true,self.nodes_true=self.G,self.edges,self.nodes
        #self.G_true,self.edges_true,self.nodes_true=f.add_interruptions(self.G.copy(),0.4)
        #if epicenter==None: epicenter=f.random_epicenter(self.nodes)
        #if radius==None: radius=np.random.randint(int(distance*0.5),int(distance*1.4))
        #self.G_true,self.edges_true,self.nodes_true=f.add_gaussian_interruptions(self.G.copy(),self.edges,self.nodes,epicenter,radius,strength)
        self.G_true=f.add_interruptions_from_scenario(self.G.copy(),scenario_gdf)
        with contextlib.redirect_stdout(None):
            self.edges_true,self.nodes_true = ox.graph_to_gdfs(self.G_true, nodes=True, edges=True)

        self.G_true,self.edges_true,self.nodes_true = f.add_damage(self.G_true,p_damage)
        self.edges_plot_true, self.edges_plot_orig = self.edges_true, self.edges_orig
        
        isolated_components=list(self.G_true.subgraph(c).copy() for c in nx.connected_components(self.G_true))
        self.G_true=max(isolated_components,key=len)
        isolated_components.remove(max(isolated_components,key=len))
        for ic in isolated_components:
            self.G.remove_nodes_from(list(ic.nodes))
            self.G.remove_edges_from(list(ic.edges))
            self.G_orig.remove_nodes_from(list(ic.nodes))
            self.G_orig.remove_edges_from(list(ic.edges))

        self.G_true.remove_nodes_from(list(nx.isolates(self.G_true)))
        self.G.remove_nodes_from(list(nx.isolates(self.G_true)))
        self.G_orig.remove_nodes_from(list(nx.isolates(self.G_true)))

        #This shouldn't be needed, the structure should be changed
        with contextlib.redirect_stdout(None):
            self.nodes_true,self.edges_true = ox.graph_to_gdfs(self.G_true, nodes=True, edges=True)
            self.nodes,self.edges = ox.graph_to_gdfs(self.G, nodes=True, edges=True)
            self.nodes_orig,self.edges_orig = ox.graph_to_gdfs(self.G_orig, nodes=True, edges=True)

        self.center=f.nodes_centroid(self.nodes)
        self.epicenter=epicenter
        self.radius=radius
        self.crs=self.nodes.crs
        
        #Population
        population_italy=f.population_from_csv('./data/popolazione_italia.csv',self.crs)
        #self.population_df=f.population_from_csv('./data/popolazione_'+ticker+'.csv',self.nodes.crs)
        area=self.area
        #area=self.center.buffer(distance*0.7)
        self.pop=population_italy[population_italy.geometry.apply(area.contains)]
        self.total_population=self.pop.popolazione.sum()
        
        #Start and end points
        #Start and end points must be tuple of coordinates
        #self.objectives=list(np.random.choice(self.G.nodes,self.n_objectives)) #list of nodes osmid
        #self.objectives=list(f.get_gaussian_victims(self.G,self.nodes,epicenter,radius,0.005).osmid.values)
        #self.objectives=self.pop
        #self.objectives['injured']=self.objectives.apply(
        #    lambda row: f.n_injured(row.geometry, row.popolazione,self.epicenter,self.radius),
        #    axis=1)
        self.objectives=f.get_victims_from_scenario(self.scenario_gdf,self.pop)
        self.n_injured=self.objectives.injured.sum()

        if ticker is not None:
            self.ES=f.get_ES(ticker,self.nodes.crs,self.nodes)
            self.ES_osmid=f.get_ES_osmid(ticker,self.nodes.crs,self.nodes)
            self.ES_dataframe=f.get_ES_df(ticker,self.nodes.crs)
            self.hospital=self.ES['ES2']
            self.hospital_gpd=gpd.GeoDataFrame([self.hospital],columns=['geometry'])
            self.start=self.ES['ES'+'2']
        else:
            self.start=f.get_hospital(point,distance,self.nodes)

        try: self.start,starting_edge=f.project_on_road(self.start,self.edges_true)
        except: pass
        self.start_gpd=gpd.GeoDataFrame([self.start],columns=['geometry'])

        #Agents
        self.agents=[agent(self.G,self.edges,self.nodes,self.start) for i in range(self.n_rescuers)]  #list of istantiations of the agent class
        for a in self.agents:
            if len(self.objectives)==0: break
            try: 
                obj=self.objectives.copy().iloc[0]
                self.objectives=self.objectives.iloc[1:]
                a.set_objective(obj)
            except nx.exception.NetworkXNoPath:
                pass
        self.global_step=0
            
    def step(self):
        """
        Makes a computational step in the simulation
        """
        for a in sample(self.agents,len(self.agents)):
            hasreached=a.step(self.G_true)
            if hasreached==True and len(self.objectives)!=0:
                while len(self.objectives)>0:
                    try:
                        obj=self.objectives.copy().iloc[0]
                        self.objectives=self.objectives.iloc[1:]
                        a.set_objective(obj)
                        break
                    except nx.exception.NetworkXNoPath:
                        pass
            elif type(hasreached)==nx.classes.multigraph.MultiGraph:
                self.G=hasreached
                self.edges=ox.graph_to_gdfs(self.G, nodes=False, edges=True)
                for aa in self.agents:
                    aa.G=self.G
                    aa.edges=self.edges
                    try:
                        aa.set_objective(aa.objectives.copy().iloc[-1])
                        break
                    except nx.exception.NetworkXNoPath:
                        pass
        
        self.global_step+=1
            
    def rescue_operation(self):
        while len(self.objectives)>0 or any([a.hasobjective for a in self.agents]):
            step_result=self.step()

    #-----Attribute methods-----
    @property
    def n_saved(self):
        n_saved=0
        for a in self.agents:
            n_saved+=a.objectives[a.objectives.success==True].injured.sum()
        return n_saved
    @property
    def n_not_saved(self):
        n_not_saved=0
        for a in self.agents:
            n_not_saved+=a.objectives[a.objectives.success==False].injured.sum()
        return n_not_saved
    @property
    def o_reach(self):
        o_reach=0
        for a in self.agents:
            o_reach+=a.objectives[a.objectives.success==True].shape[0]
        return o_reach
    @property
    def o_nonreach(self):
        o_nonreach=0
        for a in self.agents:
            o_nonreach+=a.objectives[a.objectives.success==False].shape[0]
        return o_nonreach

    @property
    def objectives_tot(self):
        objectives_tot=self.objectives
        for a in self.agents:
            objectives_tot=objectives_tot.append(a.objectives)
        return objectives_tot


    
    #-----Plotting methods and recap-----
    def print_stats(self):
        print("Injured saved: ", self.n_saved)
        print("Injured not reachable:", self.n_not_saved)
        print("Number of objectives reached: ", self.o_reach)
        print("Number of objectives not reachable: ", self.o_nonreach)
        print("Objectives waiting to be reached: ", self.objectives.shape[0])
    
    def print_stats_to_file(self,target):
        results = self.stats_to_df()
        results.to_csv(target,header=True,index=False)

    def stats_to_df(self):
        results=pa.DataFrame([[self.n_saved,self.n_not_saved,self.n_saved/(self.n_saved+self.n_not_saved),
                              *list(self.seismic_pars.values())]],
                             columns=['saved','not_saved','efficiency',
                                      *list(self.seismic_pars.keys())])
        return(results)
        
    def plot_map(self):
        ax=self.edges_plot_orig.plot(color='red',zorder=0)
        #ax=self.edges.plot(color='grey',zorder=1,ax=ax) #tratti di strada visti dai soccorritori
        ax=self.edges_plot_true.plot(color='green',zorder=2,ax=ax)
        ax=gpd.GeoDataFrame([self.start],columns=['geometry']).plot(ax=ax,color='red',markersize=20,zorder=2)
        ax=self.objectives_tot.plot(ax=ax,color='b',markersize=15,zorder=2)
        plt.show()
        return 0

    def plot_scenario(self):
        ax=self.scenario_gdf.plot(column='intensity_normalized',zorder=0,cmap='inferno')
        ax=self.edges_plot_orig.plot(color='red',zorder=1,ax=ax)
        ax=self.edges_plot_true.plot(color='green',zorder=2,ax=ax)
        plt.show()

    def plot_dynamics(self):
        ax=self.edges_plot_orig.plot(color='red',zorder=0)
        ax=self.edges.plot(color='grey',zorder=1,ax=ax)
        ax=self.edges_plot_true.plot(color='green',zorder=2,ax=ax)
        ax=gpd.GeoDataFrame([self.start],columns=['geometry']).plot(ax=ax,color='red',markersize=20,zorder=2)
        for a in self.agents:
            ax=a.traj_df.plot(ax=ax,color='blue',markersize=5,zorder=2)
            #ax=gpd.GeoDataFrame(a.objectives,columns=['geometry']).plot(ax=ax,color='orange',zorder=3)
            ax=gpd.GeoDataFrame(a.objectives,geometry='geometry').plot(ax=ax,column='success',zorder=3)
        plt.show()
            
    def animate_dynamics(self):
        
        for i in range(0,self.global_step,4):
            ax=self.edges_orig.plot(color='red',zorder=1)
            ax=self.edges_true.plot(color='green',zorder=2,ax=ax)
            
            self.hospital_gpd.plot(ax=ax,color='red',markersize=20,zorder=3)

            for a,c in zip(self.agents,range(len(self.agents))):
                ax=a.traj_df().iloc[i:i+1].plot(ax=ax,zorder=3,color='blue')
                ax=a.objectives.plot(ax=ax,zorder=3,color='orange')#,c=c/len(self.agents) )

            plt.axis('off')
            filepath = os.path.join('./trajectory/', 'traj.'+str(i)+'.png')
            plt.savefig(filepath,dpi=200)
            plt.close('all')
        
        return 0











#-----Experimental section-----
def get_route(start,end,G,edges,nodes):
    """
    Parameters
    ---------
    start: Start point, in Shapely point format
    end: End point, in Shapely point format
    * Something that describes the map

    Returns
    -------
    List of Shapely Points describing the route
    """

    if start.distance(end)<300:
        return [],[start,end],[]

    end_onroad,closest_street_end=f.project_on_road(end,edges)
    start_onroad,closest_street_start=f.project_on_road(start,edges)

    # print('DDDDD')
    # #debug
    # ax=edges.plot(color='grey',zorder=1)
    # ax=gpd.GeoDataFrame([start,end],columns=['geometry'],crs=nodes.crs).plot(
    #     ax=ax,zorder=2,color='green')
    # ax=gpd.GeoDataFrame([end],columns=['geometry'],crs=nodes.crs).plot(
    #     ax=ax,zorder=3,color='blue')
    # ax=gpd.GeoDataFrame([start_onroad,end_onroad],columns=['geometry'],crs=nodes.crs).plot(
    #     ax=ax,zorder=2,color='red')
    # ax=gpd.GeoDataFrame([closest_street_start ,closest_street_end],columns=['geometry'],crs=nodes.crs).plot(
    #     ax=ax,zorder=2,color='#f39c12')
    # plt.show()
    
    #Get the route between the two nodes approximating the trajectory
    start_osmid = f.get_nearest_node(start_onroad, nodes)
    end_osmid = f.get_nearest_node(end_onroad, nodes)
    try:
        # print('BBBBB')
        nodetonode_route = nx.shortest_path(G,start_osmid,end_osmid)
        # print('EEEEE')
    except:
        traceback.print_exc()
        # print('CCCCCCC')
        # nodetonode_route = nx.shortest_path(G,start_osmid,end_osmid)
        # print(nodetonode_route)
        # ax=edges.plot(color='grey',zorder=1)
        # ax=gpd.GeoDataFrame([start,end],columns=['geometry'],crs=nodes.crs).plot(
        #     ax=ax,zorder=2,color='red')
        # ax=nodes[nodes.osmid==start_osmid].plot(ax=ax,zorder=2,color='blue')
        # ax=nodes[nodes.osmid==end_osmid].plot(ax=ax,zorder=2,color='green')
        # plt.show()
        raise nx.exception.NetworkXNoPath 

    if len(nodetonode_route)<2: nodetonode_route_points = []
    else:
        nodetonode_route_points = route_to_rp(nodetonode_route,edges)
        #Check if the nearest node is also the most convenient
        last_edge=f.fetch_edge(edges,nodetonode_route[-1],nodetonode_route[-2])
        if last_edge.intersects(end_onroad.buffer(0.0001)): nodetonode_route.pop(-1) ##PROBLEM HERE
        first_edge=f.fetch_edge(edges,nodetonode_route[0],nodetonode_route[1])
        if first_edge.intersects(start_onroad.buffer(0.0001)): nodetonode_route.pop(0)
        #plt.plot(*first_edge.xy,zorder=1)
        #plt.plot(*start_onroad.xy,zorder=2,color='r')
        #plt.show()
        
    #Get the points connecting the reachable nodes to the precise points of the road
    start_route_points=f.intermediate_route_points(start_onroad,closest_street_start,nodetonode_route_points[0])
    if start_route_points[0].distance(start)>start_route_points[-1].distance(start):
        start_route_points=start_route_points[::-1]
    start_route_points.insert(0,start)
    end_route_points=f.intermediate_route_points(end_onroad,closest_street_end,nodetonode_route_points[-1])
    if end_route_points[0].distance(end)<end_route_points[-1].distance(end):
        end_route_points=end_route_points[::-1]
    end_route_points.append(end)
    end_route_points.extend(end_route_points[::-1])
    
    #Merge the route points
    route_points=start_route_points+nodetonode_route_points+end_route_points

    #Debug
    if debug >= 2:
        ax=edges.plot(color='grey',zorder=1)
        ax=gpd.GeoDataFrame(start_route_points,columns=['geometry'],crs=nodes.crs).plot(
            ax=ax,zorder=2,color='#f1c40f')
        ax=gpd.GeoDataFrame(end_route_points,columns=['geometry'],crs=nodes.crs).plot(
            ax=ax,zorder=2,color='#e67e22')
        ax=gpd.GeoDataFrame(nodetonode_route_points,columns=['geometry'],crs=nodes.crs).plot(
            ax=ax,zorder=2,color='#f39c12')
        ax=gpd.GeoDataFrame([start,start_onroad,end_onroad,end],columns=['geometry'],crs=nodes.crs).plot(
            ax=ax,zorder=3,markersize=15,color='r')
        plt.show()

    return nodetonode_route, start_route_points, end_route_points


