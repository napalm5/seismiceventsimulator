places_dict={
    'cariati' : {
        'place' : 'Cariati,Italy',
        'point' : (39.495648, 16.947610),
        'distance' : 9000,
        #'point' : (39.459123,16.829338369849822),
        #'distance' : 15000,

    },
    'prato' : {
        'place' : 'Prato,Italy',
        'point' : (43.969333,11.070333),
        'distance' : 18000,
    },
    'ginosa' : {
        'place' : 'Ginosa,Italy',
        'point' : (40.52,16.815),
        'distance' : 16000,
    },
    'vallata' : {
        'place' : 'Vallata,Italy',
        'point' : (41.02,15.35),
        'distance' : 16000,
    },
    'lentini' : {
        'place' : 'Lentini,Italy',
        'point' : (37.295,14.965),
        'distance' : 14000,
    }

}

