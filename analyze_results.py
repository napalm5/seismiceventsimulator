#!/bin/python

import glob
import pandas as pa
import numpy as np
import matplotlib.pyplot as plt

data_files = glob.glob('./run_*'+'/results.csv')

dfs = []
results = pa.DataFrame([])

for i,file_name in enumerate(data_files):
    print(file_name)

    df = pa.read_csv(file_name, header=0)
    dfs.append(df)

    results=results.append(df)

print(results)
