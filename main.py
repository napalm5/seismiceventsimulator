#!/usr/bin/env python3

import shapely as shp
import geopandas as gpd
from shapely.geometry import Point

import osmnx as ox
import networkx as nx
ox.config(log_file=True, log_console=True, use_cache=True)
#from LatLon import LatLon

import numpy as np
import scipy as sp
import pandas as pa
from sklearn.preprocessing import MinMaxScaler
from matplotlib import pyplot as plt

import os
import sys
from importlib import reload
import logging
from xml.dom import minidom
import configparser as cp
#logging.getLogger(ox.__name__).setLevel(logging.WARNING)
ox_log=logging.getLogger(ox.__name__)
ox_log.addHandler(logging.NullHandler())
ox_log.propagate = False
from modules import functions as f
from modules import classes as c
from modules import places

par=f.check_args(sys.argv[1:])
# See https://stackoverflow.com/questions/15959534/visibility-of-global-variables-in-imported-modules
import builtins
builtins.debug=par.debug
RESULTS_DIR=par.folder


damage_df=pa.read_csv(RESULTS_DIR+'/gmf-data.csv')
area_gdf=gpd.read_file(RESULTS_DIR+'/area.shp')
area_crs=area_gdf.crs
sites_df=pa.read_csv(RESULTS_DIR+'/sitemesh.csv')
sites_gdf=sites_df.copy()
sites_gdf['geometry']=[Point(lon,lat) for lon,lat in zip(sites_df.lon.values,sites_df.lat.values)]
sites_gdf=gpd.GeoDataFrame(sites_gdf, crs={'init': 'epsg:4326'}).to_crs({'init': 'epsg:32633'}) #This df will be deleted

def link_id(row,sites_gdf):
    for n,sr in sites_gdf.iterrows():
        if row.geometry.contains(sr.geometry):
            row['FID'] = int(sr['site_id'])
            sites_gdf.drop(n,inplace=True)
            break
    return row

area_gdf = area_gdf.apply(lambda x: link_id(x,sites_gdf), axis=1)
area_gdf.index = area_gdf.FID
area_gdf.sort_index(inplace=True)
area_gdf.crs=area_crs

scenario_gdf = area_gdf.copy()
scenario_gdf['intensity'] = damage_df['gmv_PGA']
scaler = MinMaxScaler()
scenario_gdf['intensity_normalized'] = scaler.fit_transform(scenario_gdf['intensity'].values.reshape(-1,1))
#scenario_gdf.plot(column='intensity',cmap='hot',legend=True)

#Reading seismic parameters
xmldoc = minidom.parse(RESULTS_DIR+'/rupture_model.xml')
magnitude = float(xmldoc.getElementsByTagName('magnitude')[0].childNodes[0].data)
scenario_ini = cp.ConfigParser()
scenario_ini.read(RESULTS_DIR+'/scenario.ini')
ticker=scenario_ini['general']['ticker']
seismic_pars={'magnitude' : magnitude, 'ticker' : ticker}

u=c.simulation(ticker=par.ticker,scenario_gdf=scenario_gdf,seismic_pars=seismic_pars)

u.print_stats()
u.rescue_operation()
#u.plot_dynamics()
u.print_stats()
u.print_stats_to_file(RESULTS_DIR+'/results.csv')

