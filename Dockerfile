FROM python:3.7.4-slim-buster

COPY requirements.txt /

RUN apt update
RUN apt install -y gcc libc-dev libgeos-dev libgdal-dev libspatialindex-c5
RUN pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org -r /requirements.txt

ADD modules.tar.gz /app
ADD data.tar.gz /app
COPY main.py percolation.py /app/
WORKDIR /app

CMD bash
#RUN echo "@community http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
#RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
#RUN apk update
#RUN apk add --no-cache gcc libc-dev geos-dev gdal-dev py-pandas

CMD bash
#CMD ["python", "main.py"]
