#!/usr/bin/env python3
import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
from importlib import reload
import logging
import itertools as it
import sys
import time

#from modules import functions as f

results_df=pa.read_csv('./results/data_c.csv',header=None)
results_df.columns=['ticker','minutes','reps','p_damage','n_saved','correlated']
#results_df.p_damage=results_df.p_damage.astype(float)
repetitions=results_df.reps.values[0]
cts=results_df.ticker.unique()

ax=plt.scatter(results_df.p_damage.values,results_df.n_saved.values,s=3)

#ax.set_axisbelow(True)
plt.grid(True, color="#93a1a1", alpha=0.3)
plt.xlabel(r'$n_{danni}$')
plt.ylabel(r'$n_{salvati}$')

plt.savefig('./plots/correlated_damage.png')
plt.show()
