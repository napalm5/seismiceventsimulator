#!/usr/bin/env python3
import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
from importlib import reload
import logging
import itertools as it
import sys
import time

from modules import functions as f

results_df=pa.read_csv('./results/data.csv',header=None)
results_df.columns=['ticker','minutes','reps','p_damage','n_saved','correlated']
#results_df.p_damage=results_df.p_damage.astype(float)
repetitions=results_df.reps.values[0]
cts=results_df.ticker.unique()

for min in results_df.minutes.unique():
    plot_df=results_df[results_df['minutes']==min]
#    plot_df.sort_values(by='p_damage')
    print(plot_df)
    fig, ax = plt.subplots(figsize=(6, 5))
    x=plot_df[plot_df.ticker==cts[0]].p_damage.values
    Y=[plot_df[plot_df.ticker==ct].n_saved.values  for ct in cts]
    Y=[y[np.argsort(x)] for y in Y]
    x=np.sort(x)
    print(x)
    print(Y)
    K=cts
    aucs=[]
    for y,k in zip(Y,K):
        # if i==1:
        #     y[1]=y[3]
        #     y[2]=y[-1]
        #     y[3]=0
        #     y[-1]=0
        #     print(y)
        ax.plot(x,y,'o-',label=str(k),linewidth=1.5, linestyle='dashed',markersize=8)
        aucs.append(round(f.auc(x,y),6))
    
    handles, labels = ax.get_legend_handles_labels()
    
    leg = plt.legend(handles=handles, labels=[r' '+ticker+', auc='+str(auc) for ticker,auc in zip(labels,aucs)])
    #title=r'$\bar{o}$',fancybox=0,loc=0,borderaxespad=0.)
    leg.get_frame().set_edgecolor('black')
    plt.savefig('./plots/percolation_damage_r'+str(repetitions)+'_m'+str(min)+'.pdf')

plt.show()


ax.set_axisbelow(True)
plt.grid(True, color="#93a1a1", alpha=0.3)
plt.xlabel(r'Tempo massimo (m)')
plt.ylabel(r'$n_{salvati}$')
