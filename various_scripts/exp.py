#!/usr/bin/env python3

import shapely as shp
import geopandas as gpd
from shapely.geometry import Point

import osmnx as ox
import networkx as nx
ox.config(log_file=True, log_console=True, use_cache=True)
#from LatLon import LatLon

import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
import sys
from importlib import reload
import logging
#logging.getLogger(ox.__name__).setLevel(logging.WARNING)
ox_log=logging.getLogger(ox.__name__)
ox_log.addHandler(logging.NullHandler())
ox_log.propagate = False
from modules import functions as f
from modules import classes as c
from modules import places

par=f.check_args(sys.argv[1:])

if par.ticker is not None:
    place=places.places_dict[par.ticker]['place']
    point=places.places_dict[par.ticker]['point']
    distance=places.places_dict[par.ticker]['distance']
    earthquake_radius=distance/4
    earthquake_strength=np.random.uniform(0.2,1.)
else:
    point=(41.548920, 14.622994)
    earthquake_radius=600
    earthquake_strength=np.random.uniform(0.2,1.)
    distance=earthquake_radius*4
#point=(39.489971, 16.958957)


#OO Simulation
u=c.simulation(point,distance,par.ticker,0.1,0)
u.rescue_operation()
print("Saved ",u.n_saved," people out of ", u.n_injured," injured, in an area inhabited by ",u.total_population," people")

#u.plot_dynamics()

'''
prato=( 43.969333, 11.070333 )
epsg_prato=32633

#Trajectorying
#get route as linestring
ls_list=[]
for u,v in zip(route,route[1:]):
    ls=edges[(edges['u']==u) & (edges['v']==v)].geometry.values[0] if len(edges[(edges['u']==u) & (edges['v']==v)].geometry.values)!=0 else edges[(edges['v']==u) & (edges['u']==v)].geometry.values[0]
    ls_list.append(ls)
    
route_line=shp.ops.linemerge(ls_list)
route_geom = gpd.GeoDataFrame([[route_line]], geometry='geometry', crs=edges.crs, columns=['geometry'])
route_points=[shp.geometry.Point(p) for p in route_line.coords]

#Moving the agent
stride=100.
pos=route_points.pop(0)
traj=[pos]

def step(pos,stride,route_points):
    while stride is not 0:
        if pos.distance(route_points[0])>stride:
            #print('not reached, distance = ',pos.distance(route_points[0]))
            ls=shp.geometry.LineString((pos,route_points[0]))
            pos=ls.interpolate(stride)
            stride=0
        else:
            #print('reached, distance = ',pos.distance(route_points[0]))
            stride-=pos.distance(route_points[0])
            pos=route_points.pop(0)
            if len(route_points)==0: return pos
    return pos


while len(route_points)>0 and len(traj)<10000:
    pos=step(pos,stride,route_points)
    traj.append(pos)

traj_gpd=gpd.GeoDataFrame(traj,geometry='geometry', crs=nodes.crs, columns=['geometry'])
            
#f.plot_traj(traj_gpd,edges,nodes,buildings)
#f.animate_traj(traj_gpd,edges,nodes,buildings)

'''


