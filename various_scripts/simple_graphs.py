#!/usr/bin/env python3

import shapely as shp
import geopandas as gpd
from shapely.geometry import Point

import osmnx as ox
import networkx as nx
ox.config(log_file=True, log_console=True, use_cache=True)
#from LatLon import LatLon

import random
import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
import sys
from importlib import reload
import logging
#logging.getLogger(ox.__name__).setLevel(logging.WARNING)
ox_log=logging.getLogger(ox.__name__)
ox_log.addHandler(logging.NullHandler())
ox_log.propagate = False
from modules import functions as f
from modules import classes as c
from modules import places

par=f.check_args(sys.argv[1:])

'''
pol_triangolo=shp.geometry.Polygon([[  11.071339,43.889407 ],[ 11.072283,43.888472 ],[  11.072959 ,43.889369]])
#debug
plt.plot(*pol_triangolo.exterior.xy)
plt.show()
G_triangolo=ox.graph_from_polygon(pol_triangolo)
G_triangolo=ox.get_undirected(G_triangolo)
ox.plot_graph(G_triangolo)
G_triangolo.remove_edge(*list(G_triangolo.edges)[0])
ox.plot_graph_route(G_triangolo,nx.shortest_path(
    G_triangolo,
    list(G_triangolo.nodes)[0],
    list(G_triangolo.nodes)[1]))
'''

##
#Pettine
pol_pettine=shp.geometry.Polygon([[ 11.067640 ,43.881869],[11.070256,43.883678],[ 11.085401 , 43.877212],[   11.083423 ,43.875240]])
G_pettine=ox.graph_from_polygon(pol_pettine)
G_pettine=ox.get_undirected(G_pettine)
#ox.plot_graph(G_pettine)

#Griglia
pol_griglia=shp.geometry.Polygon([[  11.077138,43.892128],[  11.082752,43.896297],[   11.093654,43.89129],[  11.089588 ,43.888179]])
#plt.plot(*pol_griglia.exterior.xy)
#plt.show()
G_griglia=ox.graph_from_polygon(pol_griglia)
G_griglia=ox.get_undirected(G_griglia)
#ox.plot_graph(G_griglia)

polygon_list=[pol_pettine,pol_griglia]
geometries_list=[G_pettine,G_griglia]
key_list=['pettine','griglia']
fig2, ax2 = plt.subplots(figsize=(6, 5))
fig, ax = plt.subplots(figsize=(6, 5))

for G,p,k in zip(geometries_list,polygon_list,key_list):
    start=ox.get_nearest_node(G,
            (p.exterior.xy[0][0],p.exterior.xy[1][0]))

    gtmp=list(G.nodes)
    end_list=random.sample(gtmp,10)

    nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
    ax=edges.plot(ax=ax,zorder=1,color='grey')
    ax=nodes[nodes.osmid==start].plot(ax=ax,zorder=3,color='blue')
    ax=nodes[nodes.osmid.isin(end_list)].plot(ax=ax,zorder=2,color='red')
    fig.show()
    
    pdp={}
    for p_damage in (0.,0.05,0.1,0.2,0.3,0.4,0.5,0.75,1.):
        p_percolation=f.p_percolation_better(G.copy(),start,end_list,p_damage,n_iterations=100)
        print('The graph is ',p_percolation,' well-connected, for damage ',p_damage)
        pdp.update({p_damage:p_percolation})
    
    ax2.plot(pdp.keys(),pdp.values(),'o-',label=k,linewidth=1.5, linestyle='dashed',markersize=8)

handles, labels = ax2.get_legend_handles_labels()
leg = ax2.legend(handles=handles, labels=[k for k in key_list])
leg.get_frame().set_edgecolor('black')

fig2.show()
    
'''
if par.ticker is not None:
    place=places.places_dict[par.ticker]['place']
    point=places.places_dict[par.ticker]['point']
    distance=places.places_dict[par.ticker]['distance']
    earthquake_radius=distance/4
    earthquake_strength=np.random.uniform(0.2,1.)
else:
    point=(41.548920, 14.622994)
    earthquake_radius=600
    earthquake_strength=np.random.uniform(0.2,1.)
    distance=earthquake_radius*4
#point=(39.489971, 16.958957)


#OO Simulation
u=c.simulation(point,distance,par.ticker,0.1,0)
u.rescue_operation()
print("Saved ",u.n_saved," people out of ", u.n_injured," injured, in an area inhabited by ",u.total_population," people")

#u.plot_dynamics()
'''
'''
prato=( 43.969333, 11.070333 )
epsg_prato=32633

#Trajectorying
#get route as linestring
ls_list=[]
for u,v in zip(route,route[1:]):
    ls=edges[(edges['u']==u) & (edges['v']==v)].geometry.values[0] if len(edges[(edges['u']==u) & (edges['v']==v)].geometry.values)!=0 else edges[(edges['v']==u) & (edges['u']==v)].geometry.values[0]
    ls_list.append(ls)
    
route_line=shp.ops.linemerge(ls_list)
route_geom = gpd.GeoDataFrame([[route_line]], geometry='geometry', crs=edges.crs, columns=['geometry'])
route_points=[shp.geometry.Point(p) for p in route_line.coords]

#Moving the agent
stride=100.
pos=route_points.pop(0)
traj=[pos]

def step(pos,stride,route_points):
    while stride is not 0:
        if pos.distance(route_points[0])>stride:
            #print('not reached, distance = ',pos.distance(route_points[0]))
            ls=shp.geometry.LineString((pos,route_points[0]))
            pos=ls.interpolate(stride)
            stride=0
        else:
            #print('reached, distance = ',pos.distance(route_points[0]))
            stride-=pos.distance(route_points[0])
            pos=route_points.pop(0)
            if len(route_points)==0: return pos
    return pos


while len(route_points)>0 and len(traj)<10000:
    pos=step(pos,stride,route_points)
    traj.append(pos)

traj_gpd=gpd.GeoDataFrame(traj,geometry='geometry', crs=nodes.crs, columns=['geometry'])
            
#f.plot_traj(traj_gpd,edges,nodes,buildings)
#f.animate_traj(traj_gpd,edges,nodes,buildings)

'''


