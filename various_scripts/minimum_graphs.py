 #!/usr/bin/env python3

import shapely as shp
import geopandas as gpd
from shapely.geometry import Point

import osmnx as ox
import networkx as nx
import networkx.algorithms as na
import networkx.algorithms.approximation as naa
ox.config(log_file=True, log_console=True, use_cache=True)
#from LatLon import LatLon

import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
import sys
from importlib import reload
import pickle as pk
import logging
#logging.getLogger(ox.__name__).setLevel(logging.WARNING)
ox_log=logging.getLogger(ox.__name__)
ox_log.addHandler(logging.NullHandler())
ox_log.propagate = False
from modules import functions as f
from modules import classes as c
from modules import places

par=f.check_args(sys.argv[1:])

if par.ticker is not None:
    place=places.places_dict[par.ticker]['place']
    point=places.places_dict[par.ticker]['point']
    distance=places.places_dict[par.ticker]['distance']
    distance=10000
    earthquake_radius=distance/4
    earthquake_strength=np.random.uniform(0.2,1.)
else:
    point=(41.548920, 14.622994)
    earthquake_radius=600
    earthquake_strength=np.random.uniform(0.2,1.)
    distance=earthquake_radius*4
#point=(39.489971, 16.958957)

#OO Simulation
u=c.simulation(point,distance,par.ticker,0.1,0)
#length = nx.shortest_path_length(G=graph, source=orig_node, target=target_node, weight='length') 
process=list(u.ES_osmid.values())
sys.exit()
G=f.make_mono(u.G)  
SG=nx.MultiGraph(naa.steiner_tree(G, process,weight='time'))

ESGs=[]

for i in range(100):
    ESGs.append(f.extend_tree(SG,u.G,np.random.randint(5,100)))

meshednesses=[f.meshedness_coefficient(esg) for esg in ESGs]
resiliences=[f.resilience_indicator(esg,process) for esg in ESGs]

esgdf=pa.DataFrame({'resilience':resiliences,'meshedness':meshednesses})
#esgdf['graph']=ESGs

results_list=[ESGs,meshednesses,resiliences]
with open('results.pck','wb') as ofile:
    pk.dump(results_list,ofile)
    
# import time
# start = time.time()
# tmp=f.resilience_indicator(SG,u.ES_osmid.values())
# end = time.time()
# print('Indicator calculated in ',end-start,'s')

'''
Come prima cosa dobbiamo definire un processo, più o meno astratto, che mette in comunicazione i diversi soggetti e luoghi coinvolti nella gestione dell'emergenza.
Supponiamo, come luoghi di passaggio, gli elementi della CLE (ES1, ES2, ES3, Aamm, Aric) e anche le località abitate. A questo punto, in attesa che qualcuno ci definisca un processo reale, ne supponiamo uno: può essere il commesso viaggiatore che fa il giro delle 7 chiese oppure una qualunque altra sequenza di percorsi ragionevole.

A questo punto va definito il grafo minimo che garantisce il processo. Per questo dovremmo tenere in considerazione un dato di rischio associato a ciascun tratto stradale (o esteso su tutto il territorio in esame) che definisce la possibilità che per un dato evento che quel tratto di strade sia interrotto o meno. Ora questo dato di rischio non lo abbiamo, pertanto utilizza una matrice raster, uniforme su tutto il territorio in esame, con valori di rischio ad esempio pari a 0, matrice che dovrà cambiare e potrà determinare un grafo differente.
Il grafo minimo deve tener conto dei livelli funzionali stradali. Un'autostrada ha priorità rispetto a tutte le altre strade, una statale su una regionale, etc. Per questo puoi estrarre o supporre delle velocità di attraversamento.
Questo grafo dovrà essere qualificato con un indicatore. Su questo ti chiedo di fare delle tue ipotesi. Tale indicatore deve misurare la possibilità che il processo si completi con successo.

Poi ci viene chiesto di identificare dei livelli di ridondanza. 
Il grafo si amplia a step: anche qui ti chiedo di ipotizzare come misurare tale step: può essere una quantità di strade che migliora l'indicatore precedente di x %, può essere una x % di strade in più rispetto al grafo minimo, etc.
Immaginiamoci 2 livelli di ridondanza e per ciascuno tiriamo fuori un grafo ed un indicatore (che dovrà viavia migliorare, in quanto misura la possibilità che il sistema di emergenza funziona).

'''
