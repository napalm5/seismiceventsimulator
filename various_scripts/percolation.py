#!/usr/bin/env python3

import shapely as shp
import geopandas as gpd
from shapely.geometry import Point

import osmnx as ox
import networkx as nx
ox.config(log_file=True, log_console=True, use_cache=True)
#from LatLon import LatLon

import numpy as np
import scipy as sp
import pandas as pa
from matplotlib import pyplot as plt

import os
from importlib import reload
import logging
#logging.getLogger(ox.__name__).setLevel(logging.CRITICAL)
ox_log=logging.getLogger(ox.__name__)
ox_log.addHandler(logging.NullHandler())
ox_log.propagate = False
import itertools as it
import sys
import time

from modules import functions as f
from modules import classes as c
from modules import places

par=f.check_args(sys.argv[1:])


place=places.places_dict[par.ticker]['place']
point=places.places_dict[par.ticker]['point']
distance=places.places_dict[par.ticker]['distance']

start=time.time()




#print(f.percolation_with_population(G,start,0.0,pop))

#OO Simulation
u=c.simulation(point,distance,par.ticker,0.,0)

print('##### PREPROC ENDED #####')




percolation_damage_plot={}
results_df=pa.DataFrame()
max_rescue_time=par.minutes
es='ES'+par.es

if par.causal:
#Percolation with gaussian damage
    print('Using gaussian damage')
    for i in range(par.repetitions):
        x,y=f.connectivity_gpt(u.G,u.ES_osmid[es],max_rescue_time,u.distance)
        percolation_damage_plot.update({x:y})
        print('The hospital is ',y,' well-connected, for damage ',x)
    
else: 
    #Percolation with random damage
    print('Using random damage')
    for p_damage in (0.,0.05,0.1,0.2,0.3,0.4,0.5,0.75,1.):
        p_percolation=f.connectivity_pt(u.G,u.ES_osmid[es],p_damage,max_rescue_time,u.population_df,reps=par.repetitions)
        print('The hospital is ',p_percolation,' well-connected, for damage ',p_damage)
        percolation_damage_plot.update({p_damage:p_percolation})
        
auc=f.auc(list(percolation_damage_plot.keys()),list(percolation_damage_plot.values()))
auc=round(auc,8)
print('The territory is ',auc,' robust')

results_df['ticker']=[par.ticker for i in range(results_df.shape[0])]
results_df['minutes']=[par.minutes for i in range(results_df.shape[0])]
results_df['reps']=[par.repetitions for i in range(results_df.shape[0])]
results_df['p_damage']=percolation_damage_plot.keys()
results_df['n_saved']=percolation_damage_plot.values()
results_df['ticker']=[par.ticker for i in range(results_df.shape[0])]
results_df['minutes']=[par.minutes for i in range(results_df.shape[0])]
results_df['reps']=[par.repetitions for i in range(results_df.shape[0])]
results_df['causal']=[par.causal for i in range(results_df.shape[0])]

if not par.nooutput:
    results_df.to_csv('./results/percolation_damage_'+par.ticker+'_ES'+par.es+'_r'+str(par.repetitions)+'_m'+str(par.minutes)+'.csv',index=False,header=False)

if par.plot:
    fig, ax = plt.subplots(figsize=(6, 5))
    ax.plot(percolation_damage_plot.keys(),percolation_damage_plot.values(),'bo-',linewidth=1.5, linestyle='dashed',markersize=8)
    ax.set_axisbelow(True)
    # alpha is used to control the transparency
    plt.grid(True, color="#93a1a1", alpha=0.3)
    plt.title('AUC = '+str(auc))
    plt.xlabel(r'$p_{danno}$')
    plt.ylabel(r'$n_{salvati}$')
    plt.show()
    #ax.set_ylim(bottom=0)
    #ax.set_xlim(bottom=0)
    #plt.savefig('percolation_damage_'+par.ticker+'_'+str(par.repetitions)+'.pdf')
    
'''
# percolation_time_plot=[]
# damages=(0.005,0.025,0.05,0.1)
# times=(15,45,90,180,360)

# for p_damage in damages:
#     perc_tmp=[]
    
#     for max_rescue_time in times:
#         print(max_rescue_time,p_damage)
#         p_percolation=f.get_connectivity_pt(u.G,f.get_hospital_osmid(point,distance,u.nodes),p_damage,max_rescue_time,u.population_df)
#         perc_tmp.append(p_percolation)
        
#     percolation_time_plot.append(perc_tmp)




# print(percolation_time_plot)
# fig, ax = plt.subplots(figsize=(6, 5))
# for y,k in zip(percolation_time_plot,damages):
#     ax.plot(times,y,'o-',label=str(k),linewidth=1.5, linestyle='dashed',markersize=8)

# handles, labels = ax.get_legend_handles_labels()

# leg = plt.legend(handles=handles, labels=[r' $p_{damage}$ = '+i for i in labels])
# #title=r'$\bar{o}$',fancybox=0,loc=0,borderaxespad=0.)
# leg.get_frame().set_edgecolor('black')



# ax.set_axisbelow(True)
# plt.grid(True, color="#93a1a1", alpha=0.3)
# plt.xlabel(r'Tempo massimo (m)')
# plt.ylabel(r'$n_{salvati}$')
# plt.savefig('percolation_time_'+ticker+'.pdf')


end=time.time()
print('The program ran in ',end-start,'s')


plt.show()
    
# fig, axs = plt.subplots(2,2,figsize=(6, 5))
# for i, ax in zip(range(3),axs.flat):
#     ax.plot(times,percolation_time_plot[i],'bo-',linewidth=1.5, linestyle='dashed',markersize=8)
#     ax.set_axisbelow(True)
#     # alpha is used to control the transparency
#     #ax.set_ylim(bottom=0)
#     #ax.set_xlim(bottom=0)


'''
    
    
print("\a")

